package com.example.hometask3;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.CrudRepository;

@SpringBootApplication
public class HomeTask3Application {

    public static void main(String[] args) {
        SpringApplication.run(HomeTask3Application.class, args);
    }

    @Autowired
    CrudRepository<User,Long> userRepository;

    @PostConstruct
    void setUp(){
       User u =  userRepository.save(new User(null,"alex1","0000"));

    }

}
