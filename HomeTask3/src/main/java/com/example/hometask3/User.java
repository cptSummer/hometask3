package com.example.hometask3;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Value;
import lombok.With;

@Value
@With
public class User {

    Long id;
    String login;
    String pass;
}
