package com.example.hometask3;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
@Repository
@Slf4j
public class UserRepositoryTemplateImpl implements CrudRepository<User,Long> {
    private static final String INSERT = "INSERT INTO users (name, pass) VALUES (:login, :pass)";
    private static final String UPDATE = "UPDATE users SET name=:login,pass=:pass WHERE id=:id";
    private static final String SELECT_BY_ID = "SELECT id,name,pass FROM users WHERE id=:id";
    private final NamedParameterJdbcTemplate jdbcTemplate;


    @Override
    public User save(User entity) {
        if (entity.getId() != null){
            jdbcTemplate.update(
                    UPDATE,
                    new BeanPropertySqlParameterSource(entity)
            );
            return entity;
        }else {
            GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    INSERT,
                    new BeanPropertySqlParameterSource(entity),
                    generatedKeyHolder
            );
            Map<String,Object> map = generatedKeyHolder.getKeys();
            return entity.withId((Long) map.get("id"));
        }

    }

    @Override
    public <S extends User> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<User> findById(Long id) {
        return jdbcTemplate.query(SELECT_BY_ID, new MapSqlParameterSource("id",id),
                (rs, i ) -> new User(
                rs.getObject("id",Long.class),
                rs.getString("name"),
                rs.getString("pass")
                )).stream().findAny();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Collection<User> findAll() {
        return null;
    }

    @Override
    public Iterable<User> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends User> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public Optional<User> findByName(String name) {
        return Optional.empty();
    }
}
